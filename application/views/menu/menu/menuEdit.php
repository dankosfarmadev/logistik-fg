<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">


            <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
            <form action="<?= base_url('menu/menuEdit/' . $menuView['id']); ?>" method="post">
                <div class="form-group row">
                    <label for="email" class="col-sm-2 col-form-label">Nama menu</label>
                    <div class="col-sm-10">
                        <input type="text" class="form-control" id="menu" name="menu" value="<?= $menuView['menu']; ?>">
                        <?= form_error('menu', '<small class="text-danger pl-3">', '</small>'); ?>

                    </div>
                </div>

                <input type="hidden" class="form-control" id="id" name="id" value="<?= $menuView['id']; ?>">
                <p><?php echo $menuView['id'] ?></p>

                <div class="form-group row justify-content-end">
                    <div class="col-sm-10">
                        <button type="submit" class="btn btn-primary">Edit Menu</button>
                    </div>
                </div>


            </form>


        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->