<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <h1 class="h3 mb-4 text-gray-800"><?= $title; ?></h1>
            <section id="basic-horizontal-layouts">
                <div class="row match-height">
                    <div class="col-md-12 col-12">
                        <div class="card">
                            <div class="card-header">
                                <!-- <h4 class="card-title"><?= $title; ?></h4> -->
                            </div>
                            <div class="card-body">
                                <!-- <form class="form form-horizontal" action="<?= base_url('menu/submenuUpdate'); ?>" method="post"> -->
                                <form class="form form-horizontal" action="<?= base_url('menu/submenuEdit/' . $submenuView['id']); ?>" method="post">
                                    <div class="form-body">
                                        <?php if (validation_errors()) : ?>
                                            <div class="alert alert-danger" role="alert">
                                                <?= validation_errors(); ?>
                                            </div>
                                        <?php endif; ?>
                                        <?= $this->session->flashdata('message'); ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Title</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="title" class="form-control" name="title" placeholder="title" value="<?= $submenuView['title']; ?>">
                                                <!-- <input type="text" id="title" class="form-control" name="title" placeholder="title" value="<?= $this->input->post('title') ?? $submenuView['title'] ?>"> -->
                                                <!-- <input type="text" id="title" class="form-control" name="title" placeholder="title" value=" <?= set_value($submenuView['title']); ?>"> -->
                                                <?= form_error('title', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>URL</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="url" class="form-control" name="url" placeholder="URL" value="<?= $submenuView['url']; ?>">
                                                <?= form_error('url', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>icon</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="icon" class="form-control" name="icon" placeholder="Icon" value="<?= $submenuView['icon']; ?>">
                                                <?= form_error('icon', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Active</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <!-- <label for="active" class="col-sm-2 col-form-label custom-control-label">Active</label> -->
                                                <div class="custom-control custom-switch custom-switch-glow custom-control-inline mb-1">
                                                    <input type="checkbox" class="custom-control-input" id="active" name="active[]" <?= $submenuView['is_active'] == 1 ? 'checked' : '' ?>>
                                                    <label class="custom-control-label" for="active">
                                                    </label>
                                                </div>
                                            </div>
                                            <input type="hidden" id="id" class="form-control" name="id" placeholder="id" value="<?= $submenuView['id']; ?>">
                                            <!-- <div class="col-12 col-md-8 offset-md-4 form-group">
                                                <fieldset>
                                                    <div class="checkbox">
                                                        <input type="checkbox" class="checkbox__input" id="checkbox1" checked>
                                                        <label for="checkbox1">Remember me</label>
                                                    </div>
                                                </fieldset>
                                            </div> -->
                                            <div class="col-sm-12 d-flex justify-content-end">
                                                <button type="submit" class="btn btn-primary mr-1">Edit Submenu</button>
                                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!-- Basic Horizontal form layout section end -->
        </div>
    </div>
</div>