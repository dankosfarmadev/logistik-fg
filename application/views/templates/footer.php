<!-- BEGIN: Footer-->
<footer class="footer footer-static footer-light">
    <p class="clearfix mb-0"><span class="float-left d-inline-block"><?= date('Y'); ?> &copy; Dankos Farma</span><span class="float-right d-sm-inline-block d-none">Crafted with<i class="bx bxs-heart pink mx-50 font-small-3"></i>by<a class="text-uppercase" href="#">MSTD</a></span>
        <button class="btn btn-primary btn-icon scroll-top" type="button"><i class="bx bx-up-arrow-alt"></i></button>
    </p>
</footer>
<!-- END: Footer-->


<!-- Logout Modal-->
<!-- <div class="modal fade" id="logoutModal" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
                <div class="modal-dialog" role="document">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h5 class="modal-title" id="exampleModalLabel">Ready to Leave?</h5>
                            <button class="close" type="button" data-dismiss="modal" aria-label="Close">
                                <span aria-hidden="true">×</span>
                            </button>
                        </div>
                        <div class="modal-body">Select "Logout" below if you are ready to end your current session.</div>
                        <div class="modal-footer">
                            <button class="btn btn-secondary" type="button" data-dismiss="modal">Cancel</button>
                            <a class="btn btn-primary" href="<?= base_url('auth/logout'); ?>">Logout</a>
                        </div>
                    </div>
                </div>
            </div> -->


<!-- BEGIN: Vendor JS-->
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/vendors.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/fonts/LivIconsEvo/js/LivIconsEvo.tools.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/fonts/LivIconsEvo/js/LivIconsEvo.defaults.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/fonts/LivIconsEvo/js/LivIconsEvo.min.js"></script>
<!-- BEGIN Vendor JS-->

<!-- BEGIN: Page Vendor JS-->
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/editors/quill/quill.min.js"></script>
<!-- END: Page Vendor JS-->

<!-- BEGIN: Theme JS-->
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/configs/vertical-menu-light.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/js/core/app-menu.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/js/core/app.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/components.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/footer.js"></script>
<!-- END: Theme JS-->

<!-- BEGIN: Page JS-->
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/pages/app-email.js"></script>
<!-- END: Page JS-->

<!-- Table -->
<!-- BEGIN: Page Vendor JS-->
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/jquery.dataTables.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/dataTables.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/dataTables.buttons.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/buttons.html5.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/buttons.print.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/buttons.bootstrap4.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/pdfmake.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/tables/datatable/vfs_fonts.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/pickers/pickadate/picker.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/pickers/pickadate/picker.date.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/pickers/pickadate/picker.time.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/pickers/pickadate/legacy.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/extensions/moment.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/pickers/daterange/daterangepicker.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/extensions/jquery.steps.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/forms/validation/jquery.validate.min.js"></script>
<!-- END: Page Vendor JS-->


<!-- BEGIN: Page JS-->
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/datatables/datatable.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/pickers/dateTime/pick-a-datetime.js"></script>
<!-- END: Page JS-->


<!-- BEGIN: Page Vendor JS-->
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/extensions/swiper.min.js"></script>
<!-- END: Page Vendor JS-->
<!-- BEGIN: Page JS-->
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/pages/dashboard-ecommerce.js"></script>
<!-- <script src="<?= base_url('assets/'); ?>app-assets/js/scripts/pages/dashboard-analytics.js"></script> -->
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/charts/chart-chartjs.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/charts/chart.min.js"></script>

<!-- END: Page JS-->
<script src="https://cdnjs.cloudflare.com/ajax/libs/Chart.js/3.7.1/chart.min.js"></script>


<!--  -->
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/pages/dashboard-analytics.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/charts/apexcharts.min.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/extensions/dragula.min.js"></script>

<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/pages/app-invoice.js"></script>

<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/forms/wizard-steps.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/js/scripts/forms/form-repeater.js"></script>
<script src="<?= base_url('assets/'); ?>app-assets/vendors/js/forms/repeater/jquery.repeater.min.js"></script>


<script>
    $('.custom-file-input').on('change', function() {
        let fileName = $(this).val().split('\\').pop();
        $(this).next('.custom-file-label').addClass("selected").html(fileName);
    });

    $('.form-check-input').on('click', function() {
        const menuId = $(this).data('menu');
        const roleId = $(this).data('role');

        $.ajax({
            url: "<?= base_url('admin/changeaccess'); ?>",
            type: 'post',
            data: {
                menuId: menuId,
                roleId: roleId
            },
            success: function() {
                document.location.href = "<?= base_url('admin/roleaccess/'); ?>" + roleId;
            }
        });

    });
    // $('form#id').submit(function() {
    //     $(this).find(':input[type=submit]').prop('disabled', true);
    // });


    // $('#pickadate').pickadate({
    //     format: 'yyyy-mm-dd'
    // });
</script>

</body>
<!-- END: Body-->

</html>