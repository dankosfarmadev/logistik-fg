<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">



            <!-- Zero configuration table -->
            <section id="basic-datatable">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title"><?= $title; ?></h4>
                            </div>
                            <div class="card-body card-dashboard">
                                <?php if (($this->session->userdata('role_id') == 5) || ($this->session->userdata('role_id') == 4) || ($this->session->userdata('role_id') == 2) || ($this->session->userdata('role_id') == 1)) {
                                    null;
                                } else { ?>

                                <?php } ?>

                                <div class="table-responsive">
                                    <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                                    <?= $this->session->flashdata('message'); ?>

                                    <table class="table zero-configuration">
                                        <thead>
                                            <tr>
                                                <th>#</th>
                                                <th>Create date</th>
                                                <th>No Polisi</th>
                                                <th>Create by</th>
                                                <th>Status</th>
                                                <!-- <th>Update Date</th> -->
                                                <th>Action</th>
                                            </tr>
                                        </thead>
                                        <tbody>
                                            <?php $i = 1; ?>
                                            <?php foreach ($receive as $r) : ?>
                                                <tr>
                                                    <td scope="row">
                                                        <p class="font-small-3"><?= $i; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= date('d-m-Y H:i:s', strtotime($r['create_date'])); ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['no_polisi']; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['name']; ?></p>
                                                    </td>
                                                    <td>
                                                        <p class="font-small-3"><?= $r['status']; ?></p>
                                                    </td>
                                                    <!-- <td><?= date('d-m-Y H:i:s', strtotime($r['update_date'])); ?></td> -->
                                                    <td>
                                                        <a href="<?= base_url('erp/approvalDocDetailInspectionChecklist/') . $r['id']; ?>" class="badge badge-primary">Detail</a>
                                                    </td>
                                                </tr>
                                                <?php $i++; ?>
                                            <?php endforeach; ?>
                                        </tbody>
                                    </table>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>
            <!--/ Zero configuration table -->


        </div>
    </div>
</div>
<!-- /.container-fluid -->
</div>
<!-- End of Main Content -->