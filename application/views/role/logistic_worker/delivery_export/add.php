<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <div class="row">
                <a href="<?= base_url('worker/delivery_export')  ?>" class="btn btn-icon btn-outline-primary ml-1 mb-2 mr-1"><i class="bx bx-arrow-back"></i></a>
                <h1 class="h3 mb-2 text-gray-800"><?= $title; ?></h1>
            </div>


            <section id="icon-tabs">
                <div class="row">
                    <div class="col-12">
                        <div class="card">
                            <div class="card-header">
                                <h4 class="card-title">Delivery Forms Exports</h4>
                            </div>
                            <div class="card-body">
                                <!-- <form action="#" method="POST" class="wizard-horizontal  repeater-default"> -->
                                <!-- <?= '<form action="#" method="POST" class="wizard-horizontal  repeater-default">' . form_open_multipart('worker/deliveryAdd_export'); ?> -->

                                <!-- $attributes = array('class' => 'email', 'id' => 'myform');
                                echo form_open('email/send', $attributes); -->
                                <?php
                                // $attributes = array('class' => 'wizard-horizontal  repeater-default', 'id' => 'myform')
                                $attributes = array('class' => 'wizard  repeater-default', 'id' => 'myform', 'method' => 'POST')
                                ?>
                                <?= form_open_multipart('worker/deliveryAdd_export', $attributes) ?>
                                <!-- Step 1 -->
                                <h6>
                                    <i class="step-icon"></i>
                                </h6>
                                <!-- Step 1 end-->
                                <!-- body content step 1 -->
                                <fieldset>
                                    <div class="form-body">
                                        <?= $this->session->flashdata('message'); ?>
                                        <div class="row">
                                            <div class="col-md-4">
                                                <label>Tanggal</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <fieldset class="form-group position-relative has-icon-left">
                                                    <!-- <input type="date" name="dataPicker" class="form-control pickadate" placeholder="Select Date" value="<?= set_value('dataPicker'); ?>"> -->
                                                    <input type="date" name="dataPicker" class="form-control" placeholder="Select Date" value="<?= set_value('dataPicker'); ?>">
                                                    <div class="form-control-position">
                                                        <i class='bx bx-calendar'></i>
                                                    </div>
                                                </fieldset>
                                                <?= form_error('dataPicker', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Distributor/Customer</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="dist_cust" class="form-control" name="dist_cust" placeholder="Distributor or customer" value="<?= set_value('dist_cust'); ?>">
                                                <?= form_error('dist_cust', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Ekspedisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="ekspedisi" class="form-control" name="ekspedisi" placeholder="Ekspedisi" value="<?= set_value('ekspedisi'); ?>">
                                                <?= form_error('ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>No Polisi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="no_polisi" class="form-control" name="no_polisi" placeholder="No Plat Polisi" value="<?= set_value('no_polisi'); ?>">
                                                <?= form_error('no_polisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Suhu awal muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_awal" class="form-control" name="suhu_awal" placeholder="Suhu awal" value="<?= set_value('suhu_awal'); ?>">
                                                <?= form_error('suhu_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Suhu setelah muat (Celcius)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= set_value('suhu_setelah'); ?>">
                                                <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->
                                            <div class="col-md-4">
                                                <label>Jam awal muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_awal" class="form-control" name="jam_awal" placeholder="Jam awal" value="<?= set_value('jam_awal'); ?>">
                                                <?= form_error('jam_awal', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <!-- <div class="col-md-4">
                                                <label>Jam setelah muat</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= set_value('jam_setelah'); ?>">
                                                <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div> -->






                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Kondisi kendaraan <small class="text-muted">*</small></h4>
                                            </div>



                                            <div class="col-md-4">
                                                <label>Kebersihan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan1" value="Bersih" checked>
                                                                <label for="kebersihan1">Bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebersihan" id="kebersihan2" value="Tidak bersih">
                                                                <label for="kebersihan2">Tidak bersih</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebersihan', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bunyi</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi1" value="Berisik">
                                                                <label for="bunyi1">Berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bunyi" id="bunyi2" value="Tidak berisik" checked>
                                                                <label for="bunyi2">Tidak berisik</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bunyi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Asap Kendaraan</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap1" value="Hitam">
                                                                <label for="asap1">Hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="asap" id="asap2" value="Tidak hitam" checked>
                                                                <label for="asap2">Tidak hitam</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('asap', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Bau</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau1" value="Bau">
                                                                <label for="bau1">Bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="bau" id="bau2" value="Tidak bau" checked>
                                                                <label for="bau2">Tidak bau</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('bau', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Kebocoran</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran1" value="Bocor">
                                                                <label for="kebocoran1">Bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="kebocoran" id="kebocoran2" value="Tidak bocor" checked>
                                                                <label for="kebocoran2">Tidak bocor</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('kebocoran', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Uji Emisi/KIR</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi1" value="Ya" checked>
                                                                <label for="emisi1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="emisi" id="emisi2" value="Tidak">
                                                                <label for="emisi2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('emisi', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-4">
                                                <label>Tetesan Oli</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <ul class="list-unstyled mb-0">
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli1" value="Ya">
                                                                <label for="oli1">Ya</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                    <li class="d-inline-block mr-2 mb-1">
                                                        <fieldset>
                                                            <div class="radio">
                                                                <input type="radio" name="oli" id="oli2" value="Tidak" checked>
                                                                <label for="oli2">Tidak</label>
                                                            </div>
                                                        </fieldset>
                                                    </li>
                                                </ul>
                                            </div>
                                            <?= form_error('oli', '<small class="text-danger pl-3">', '</small>'); ?>


                                            <div class="col-md-12 mt-2 mb-2">
                                                <h4 class="card-title">Pengirim <small class="text-muted">*</small></h4>
                                            </div>


                                            <div class="col-md-4">
                                                <label>Nama Ekspedisi (Nama Pengirim)</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <input type="text" id="nama_ekspedisi" class="form-control" name="nama_ekspedisi" placeholder="Nama" value="<?= set_value('nama_ekspedisi'); ?>">
                                                <?= form_error('nama_ekspedisi', '<small class="text-danger pl-3">', '</small>'); ?>
                                            </div>
                                            <div class="col-md-4">
                                                <label>Foto Pengirim</label>
                                            </div>
                                            <div class="col-md-8 form-group">
                                                <div class="row">
                                                    <div class="col-sm-9">
                                                        <div class="custom-file">
                                                            <input type="file" class="custom-file-input" id="image" name="image" required>
                                                            <label class="custom-file-label" for="image">Choose file</label>
                                                            <?= form_error('image', '<small class="text-danger pl-3">', '</small>'); ?>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>

                                            <!-- <div class="col-sm-12 d-flex justify-content-end mt-2">
                                                    <button type="submit" class="btn btn-primary mr-1">Tambah Delivery</button>
                                                    <button type="reset" class="btn btn-light-secondary">Reset</button>
                                                </div> -->
                                        </div>
                                    </div>
                                </fieldset>
                                <hr>


                                <div class="col-md-12 mt-2 mb-2">
                                    <h4 class="card-title">Catatan <small class="text-muted"></small></h4>
                                    <small class="text-danger">(catat pada kolom ini jika terdapat produk kondisi basah/rusak dalam bentuk apapun atau terjadi
                                        penyimpangan):*</small>
                                </div>

                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Catatan</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" id="catatan" class="form-control" name="catatan" placeholder="Catatan" value="<?= set_value('catatan'); ?>">
                                        <?= form_error('catatan', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <label>No seal/segel</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" id="segel" class="form-control" name="segel" placeholder="No seal/segel" value="<?= set_value('segel'); ?>">
                                        <?= form_error('segel', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                </div>


                                <!-- body content step 1 end-->
                                <hr>

                                <div class="col-md-12 mt-2 mb-2">
                                    <h4 class="card-title">Data <small class="text-muted">*</small></h4>
                                </div>


                                <!-- Step 2 -->
                                <h6>
                                    <i class="step-icon"></i>
                                </h6>
                                <!-- Step 2 end-->
                                <!-- body content of step 2 -->
                                <div class="col-md-12" style="margin: 10px;">
                                    <div class="box box-solid">
                                        <!-- <form action="<?php echo base_url('FormLoop/post') ?>" method="post" id="SimpanData"> -->
                                        <div class="box-body">
                                            <!-- <blockquote>
                                                <p><b>Keterangan!!</b></p>
                                                <small><cite title="Source Title">Inputan Yang Ditanda Bintang Merah (<span style="color: red;">*</span>) Harus Di Isi !!</cite></small>
                                            </blockquote> -->

                                            <br>
                                            <table class="table table-bordered" id="tableLoop">
                                                <thead>
                                                    <tr>
                                                        <th class="text-center col-md-1 col-sm-1">#</th>
                                                        <th>Data delivery export</th>
                                                        <th class="col-md-2"><button class="btn btn-success btn-block" id="BarisBaru"><i class="fa fa-plus"></i> Baris Baru</button></th>
                                                    </tr>
                                                </thead>
                                                <tbody></tbody>

                                            </table>
                                        </div>
                                        <!-- <div class="box-footer">
                                                <div class="form-group text-right">
                                                    <button type="submit" class="btn btn-primary"><i class="fa fa-check"></i> Simpan</button>
                                                    <button type="reset" class="btn btn-default">Batal</button>
                                                </div>
                                            </div> -->
                                        <!-- </form> -->
                                    </div>
                                </div>

                                <hr>
                                <div class="row">
                                    <div class="col-md-4">
                                        <label>Suhu setelah muat (Celcius)</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="text" id="suhu_setelah" class="form-control" name="suhu_setelah" placeholder="Suhu setelah" value="<?= set_value('suhu_setelah'); ?>">
                                        <?= form_error('suhu_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-4">
                                        <label>Jam setelah muat</label>
                                    </div>
                                    <div class="col-md-8 form-group">
                                        <input type="time" id="jam_setelah" class="form-control" name="jam_setelah" placeholder="Jam setelah" value="<?= set_value('jam_setelah'); ?>">
                                        <?= form_error('jam_setelah', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>
                                </div>

                                <hr>

                                <div class="mx-auto text-center mb-4" style="width: 200px;">
                                    <h2 class="card-title">7 CHECKLIST INSPECTION<small class="text-muted"><br /> Pemeriksaan Kendaraan Ekspor</small></h2>
                                </div>


                                <div class="col-md-6 mb-2">
                                    <label>No Polisi :</label>
                                    <input type="text" id="no_polisi2" class="form-control" name="no_polisi2" placeholder="No Polisi" value="<?= set_value('no_polisi2'); ?>">
                                    <?= form_error('no_polisi2', '<small class="text-danger pl-3">', '</small>'); ?>
                                </div>



                                <div class="row">
                                    <div class="col-md-12 mb-2">
                                        <h4 class="card-title">1. Bagian luar<small class="text-muted"> (Outside/Undercarriage)</small></h4>
                                    </div>



                                    <div class="col-md-6">
                                        <label>a. Pada bagian kontainer jika ada perbaikan, rusak , belubang, penyok</label>
                                    </div>

                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="1a" id="1a" value="Y">
                                                        <label for="1a">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="1a" id="1a2" value="N">
                                                        <label for="1a2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('1a', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>b. Bagian rangka kontainer terlihat</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="1b" id="1b" value="Y">
                                                        <label for="1b">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="1b" id="1b2" value="N">
                                                        <label for="1b2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('1b', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>c. Pastikan tidak ada benda lain yang menempel pada kontainer</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="1c" id="1c" value="Y">
                                                        <label for="1c">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="1c" id="1c2" value="N">
                                                        <label for="1c2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('1c', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>


                                    <!-- Bagian 2 -->
                                    <!-- 2.Bagian dalam dan luar pintu (Inside/Outside Doors) -->
                                    <div class="col-md-12 mt-4 mb-2">
                                        <h4 class="card-title">2. Bagian dalam dan luar pintu<small class="text-muted"> (Inside/Outside Doors)</small></h4>
                                    </div>

                                    <div class="col-md-6">
                                        <label>a. Pastikan kunci, dan mekanisme penguncian aman dan terjamin</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="2a" id="2a" value="Y">
                                                        <label for="2a">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="2a" id="2a2" value="N">
                                                        <label for="2a2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('2a', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>b. Cek jika ada baut yang hilang</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="2b" id="2b" value="Y">
                                                        <label for="2b">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="2b" id="2b2" value="N">
                                                        <label for="2b2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('2b', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>c. Pastikan engsel pintu aman dan terjamin</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="2c" id="2c" value="Y">
                                                        <label for="2c">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="2c" id="2c2" value="N">
                                                        <label for="2c2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('2c', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>






                                    <!-- Bagian 3 -->
                                    <!-- 3. Dinding Kanan (Right Side) -->
                                    <div class="col-md-12 mt-4 mb-2">
                                        <h4 class="card-title">3. Dinding kanan<small class="text-muted"> (Right Side)</small></h4>
                                    </div>

                                    <div class="col-md-6">
                                        <label>a. Perhatikan perbaikan yang mencurigakan pada bagian kontainer</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="3a" id="3a" value="Y">
                                                        <label for="3a">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="3a" id="3a2" value="N">
                                                        <label for="3a2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('3a', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>b. Perbaikan pada dinding kontainer harus terlihat dikedua sisi dalam dan diluar</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="3b" id="3b" value="Y">
                                                        <label for="3b">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="3b" id="3b2" value="N">
                                                        <label for="3b2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('3b', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>


                                    <!-- Bagian 4 -->
                                    <!-- 4. Dinding Kiri (Left Side) -->
                                    <div class="col-md-12 mt-4 mb-2">
                                        <h4 class="card-title">4. Dinding kiri<small class="text-muted"> (Left Side)</small></h4>
                                    </div>

                                    <div class="col-md-6">
                                        <label>a. Perhatikan perbaikan yang mencurigakan pada bagian kontainer</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="4a" id="4a" value="Y">
                                                        <label for="4a">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="4a" id="4a2" value="N">
                                                        <label for="4a2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('4a', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>b. Perbaikan pada dinding kontainer harus terlihat dikedua sisi dalam dan diluar</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="4b" id="4b" value="Y">
                                                        <label for="4b">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="4b" id="4b2" value="N">
                                                        <label for="4b2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('4b', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>


                                    <!-- Bagian 5 -->
                                    <!-- 5. Dinding depan (Front Wall) -->
                                    <div class="col-md-12 mt-4 mb-2">
                                        <h4 class="card-title">5. Dinding depan<small class="text-muted"> (Front Wall)</small></h4>
                                    </div>

                                    <div class="col-md-6">
                                        <label>a. Bagian dinding terbuat dari bahan yang bergelombang</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="5a" id="5a" value="Y">
                                                        <label for="5a">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="5a" id="5a2" value="N">
                                                        <label for="5a2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('5a', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>b. Tulangan pada pada sudut bagian kiri dan kanan atas harus terlihat , jika hilang
                                            atau salah adalah tidak normal.</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="5b" id="5b" value="Y">
                                                        <label for="5b">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="5b" id="5b2" value="N">
                                                        <label for="5b2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('5b', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>c. Pastikan ventilasi terlihat</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="5c" id="5c" value="Y">
                                                        <label for="5c">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="5c" id="5c2" value="N">
                                                        <label for="5c2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('5c', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>



                                    <!-- Bagian 6 -->
                                    <!-- 6. Bagian atap (Celling/Roof) -->
                                    <div class="col-md-12 mt-4 mb-2">
                                        <h4 class="card-title">6. Bagian atap<small class="text-muted"> (Celling/Roof)</small></h4>
                                    </div>

                                    <div class="col-md-6">
                                        <label>a. Pastikan bagian rangka kontainer terlihat</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="6a" id="6a" value="Y">
                                                        <label for="6a">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="6a" id="6a2" value="N">
                                                        <label for="6a2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('6a', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>b. Pastikan ventilasi terlihat, jika ada maka tidak boleh ditutup atau dihilangkan.</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="6b" id="6b" value="Y">
                                                        <label for="6b">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="6b" id="6b2" value="N">
                                                        <label for="6b2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('6b', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>c. Pastikan tidak ada benda lain yang menempel pada kontainer</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="6c" id="6c" value="Y">
                                                        <label for="6c">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="6c" id="6c2" value="N">
                                                        <label for="6c2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('6c', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>



                                    <!-- Bagian 7 -->
                                    <!-- 7. Bagian lantai (Floor) -->
                                    <div class="col-md-12 mt-4 mb-2">
                                        <h4 class="card-title">7. Bagian lantai<small class="text-muted"> (Floor)</small></h4>
                                    </div>

                                    <div class="col-md-6">
                                        <label>a. Pastikan lantai kontainer dalam keadaan rata</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="7a" id="7a" value="Y">
                                                        <label for="7a">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="7a" id="7a2" value="N">
                                                        <label for="7a2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('7a', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>b. Pastikan lantai dalam keadaan seragam.</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="7b" id="7b" value="Y">
                                                        <label for="7b">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="7b" id="7b2" value="N">
                                                        <label for="7b2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('7b', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6">
                                        <label>c. Lihat jika ada perbaikan tidak biasa pada lantai</label>
                                    </div>
                                    <div class="col-md-6 form-group">
                                        <ul class="list-unstyled mb-0">
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="7c" id="7c" value="Y">
                                                        <label for="7c">Ya</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                            <li class="d-inline-block mr-2 mb-1">
                                                <fieldset>
                                                    <div class="radio">
                                                        <input type="radio" name="7c" id="7c2" value="N">
                                                        <label for="7c2">Tidak</label>
                                                    </div>
                                                </fieldset>
                                            </li>
                                        </ul>
                                        <?= form_error('7c', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                    <div class="col-md-6 mb-2">
                                        <label>Catatan :</label>
                                        <input type="text" id="catatan2" class="form-control" name="catatan2" placeholder="catatan" value="<?= set_value('catatan2'); ?>">
                                        <?= form_error('catatan2', '<small class="text-danger pl-3">', '</small>'); ?>
                                    </div>

                                </div>
                            </div>



                            <div class="text-center mb-4 ml-2 mr-2">
                                <small class="text-danger"><br /> **Saya telah melakukan pemeriksaan secara visual kondisi kontainer/box yang disebutkan di atas.
                                    Saya yang bertanda tangan mengkonfirmasi bahwa kontainer memiliki struktur yang kuat, kedap
                                    cuaca, tidak memiliki kompartemen palsu, dan mekanisme penguncian berada dalam keadaan
                                    baik dan terjamin dan tidak menunjukkan tanda-tanda yang dapat dirusak**</small>
                            </div>



                            <div class="col-sm-12 d-flex justify-content-end mt-2 mb-2">
                                <button type="submit" class="btn btn-primary mr-1">Tambah Delivery Export</button>
                                <button type="reset" class="btn btn-light-secondary">Reset</button>
                            </div>
                            </form>
                        </div>
                    </div>
                </div>
        </div>
        </section>








    </div>
</div>
</div>

<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/twitter-bootstrap/3.3.7/js/bootstrap.min.js"></script>

<script>
    $(document).ready(function() {
        for (B = 1; B <= 1; B++) {
            Barisbaru();
        }
        $('#BarisBaru').click(function(e) {
            e.preventDefault();
            Barisbaru();
        });

        $("tableLoop tbody").find('input[type=text]').filter(':visible:first').focus();
    });

    //     <div class="col-md-2 col-sm-12 form-group">
    //                     <label for="">Product Checklist Out </label>
    //                     <div class="col-md-2 col-sm-12 form-group">
    //                                                         <label for="gender">Gender</label>
    // <select name="pco[]" id="pco" class="form-control">
    //     <option value="Y">OK</option>
    //     <option value="N">NOK</option>
    // </select>

    function Barisbaru() {
        $(document).ready(function() {
            $("[data-toggle='tooltip']").tooltip();
        });
        var Nomor = $("#tableLoop tbody tr").length + 1;
        var Baris = '<tr>';
        Baris += '<td class="text-center">' + Nomor + '</td>';
        Baris += '<td>';
        Baris += '<small><cite title="Source Title">Batch number</cite></small>';
        Baris += '<input type="text" name="bn[]" class="form-control bn" placeholder="Batch number" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">LPN number</cite></small>';
        Baris += '<input type="text" name="lpn[]" class="form-control lpn" placeholder="LPN number" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity MB</cite></small>';
        Baris += '<input type="text" name="qty_mb[]" class="form-control qty_mb" placeholder="Quantity MB" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity per MB</cite></small>';
        Baris += '<input type="text" name="qty_per_mb[]" class="form-control qty_per_mb" placeholder="Quantity per MB" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity Pick</cite></small>';
        Baris += '<input type="text" name="qty_pick[]" class="form-control qty_pick" placeholder="Quantity Pick" required="">';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Product Checklist Out</cite></small>';
        Baris += '<select name="pco[]" id="pco" class="form-control">';
        Baris += '<option value="Y">OK</option>';
        Baris += '<option value="N">NOK</option>';
        Baris += '</select>';
        Baris += '<br/>'
        Baris += '<small><cite title="Source Title">Quantity EPM Pallet (number)</cite></small>';
        Baris += '<input type="text" name="qep[]" class="form-control qep" placeholder="Quantity EPM Pallet" required="">';
        Baris += '<br/>'
        Baris += '</td>';
        Baris += '<td class="text-center">';
        Baris += '<a class="btn btn-sm btn-danger" data-toggle="tooltip" title="Hapus Baris" id="HapusBaris"><i class="bx bxs-x-square"></i></a>';
        Baris += '</td>';

        Baris += '</tr>';

        $("#tableLoop tbody").append(Baris);
        $("#tableLoop tbody tr").each(function() {
            // $(this).find('td:nth-child(2) input').focus();
        });

    }

    $(document).on('click', '#HapusBaris', function(e) {
        e.preventDefault();
        var Nomor = 1;
        $(this).parent().parent().remove();
        $('tableLoop tbody tr').each(function() {
            $(this).find('td:nth-child(1)').html(Nomor);
            Nomor++;
        });
    });

    // $(document).ready(function() {
    //     $('#SimpanData').submit(function(e) {
    //         e.preventDefault();
    //         biodata();
    //     });
    // });

    function biodata() {
        $.ajax({
            url: $("#SimpanData").attr('action'),
            type: 'post',
            cache: false,
            dataType: "json",
            data: $("#SimpanData").serialize(),
            success: function(data) {
                if (data.success == true) {
                    $('.first_name').val('');
                    $('.last_name').val('');
                    $('#notif').fadeIn(800, function() {
                        $("#notif").html(data.notif).fadeOut(5000).delay(800);
                    });
                } else {
                    $('#notif').html('<div class="alert alert-danger">Data Gagal Disimpan</div>')
                }
            },

            error: function(error) {
                alert(error);
            }

        });
    }
</script>








<!-- 

<fieldset>
    <div data-repeater-list="group-a">
        <div data-repeater-item>
            <div class="row justify-content-between">
                <div class="col-md-2 col-sm-12 form-group">
                    <label for="">Batch number </label>
                    <input type="text" class="form-control" id="bn" name="bn[]" placeholder="Batch number">
                </div>
                <div class="col-md-2 col-sm-12 form-group">
                    <label for="">LPN number </label>
                    <input type="text" class="form-control" id="lpn" name="lpn[]" placeholder="LPN number">
                </div>
                <div class="col-md-2 col-sm-12 form-group">
                    <label for="">Quantity MB </label>
                    <input type="text" class="form-control" id="qty_mb" name="qty_mb[]" placeholder="Quantity MB">
                </div>
                <div class="col-md-2 col-sm-12 form-group">
                    <label for="">Quantity per MB </label>
                    <input type="text" class="form-control" id="qty_per_mb" name="qty_per_mb[]" placeholder="Quantity per MB">
                </div>
                <div class="col-md-2 col-sm-12 form-group">
                    <label for="">Quantity Pick </label>
                    <input type="text" class="form-control" id="qty_pick" name="qty_pick[]" placeholder="Quantity Pick">
                </div>
                <div class="col-md-2 col-sm-12 form-group">
                    <label for="">Product Checklist Out </label>
                    <div class="col-md-2 col-sm-12 form-group">
                                                        <label for="gender">Gender</label>
<select name="pco[]" id="pco" class="form-control">
    <option value="Y">OK</option>
    <option value="N">NOK</option>
</select>
</div>
<input type="text" class="form-control" id="pco[]" placeholder="Product Checklist Out">
</div>
<div class="col-md-2 col-sm-12 form-group">
    <label for="">Quantity EPM Pallet </label>
    <input type="text" class="form-control" id="qep" name="qep[]" placeholder="Quantity EPM Pallet">
</div>
<div class="col-md-2 col-sm-12 form-group d-flex align-items-center pt-2">
    <button class="btn btn-danger text-nowrap px-1" data-repeater-delete type="button"> <i class="bx bx-x"></i>
        Delete
    </button>
</div>
</div>
<hr>
</div>
</div>
<div class="form-group">
    <div class="col p-0">
        <button class="btn btn-primary" data-repeater-create type="button"><i class="bx bx-plus"></i>
            Add
        </button>
    </div>
</div>
</fieldset> 