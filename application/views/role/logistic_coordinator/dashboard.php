<div class="app-content content">
    <div class="content-overlay"></div>
    <div class="content-wrapper">
        <div class="content-header row">
        </div>
        <div class="content-body">
            <?= $this->session->flashdata('message_email'); ?>

            <!-- Columns section start -->
            <section id="columns">
                <div class="row">
                    <div class="col-12 mt-3 mb-1">
                        <h4 class="text-uppercase"><?= $title; ?></h4>
                    </div>
                </div>
                <div class="row">
                    <div class="col-md-12 mt-1">
                        <div class="card-columns">
                            <div class="card">
                                <img class="card-img-top img-fluid" src="<?= base_url('assets/'); ?>app-assets/images/slider/09.png" alt="Card image cap">
                                <div class="card-header">
                                    <h4 class="card-title">Card title</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        Some quick example text to build on the card title and make up the bulk
                                        of the card's content.
                                    </p>
                                    <a href="javascript:void(0);" class="btn btn-outline-primary">Go somewhere</a>
                                </div>
                            </div>
                            <div class="card bg-primary text-center">
                                <div class="card-body">
                                    <img src="<?= base_url('assets/'); ?>app-assets/images/elements/iphone-x.png" alt="element 05" width="150" class="mb-1 img-fluid">
                                    <h4 class="card-title text-white">iPhone 11</h4>
                                    <p class="card-text text-white">945 items</p>
                                </div>
                            </div>
                            <div class="card position-static text-white bg-danger bg-lighten-1 text-center">
                                <div class="card-body">
                                    <img src="<?= base_url('assets/'); ?>app-assets/images/elements/ipad-pro.png" alt="element 02" width="120" class="mb-1 img-fluid">
                                    <h4 class="card-title text-white">iPad Mini</h4>
                                    <p class="card-text">456 items</p>
                                </div>
                            </div>
                            <div class="card">
                                <div class="card-header">
                                    <h4 class="card-title">Bottom Image Cap</h4>
                                </div>
                                <div class="card-body">
                                    <p class="card-text">
                                        Jelly-o sesame snaps cheesecake topping. Cupcake fruitcake macaroon
                                        donut pastry gummies tiramisu chocolate bar muffin. Dessert bonbon caramels brownie
                                        chocolate bar chocolate tart dragée.
                                    </p>
                                    <p class="card-text">
                                        Cupcake fruitcake macaroon donut pastry gummies tiramisu chocolate bar
                                        muffin.
                                    </p>
                                    <small class="text-muted">Last updated 3 mins ago</small>
                                </div>
                                <img class="card-img-bottom img-fluid" src="<?= base_url('assets/'); ?>app-assets/images/slider/04.jpg" alt="Card image cap">
                            </div>
                            <div class="card text-white">
                                <img class="card-img img-fluid position-sticky" src="<?= base_url('assets/'); ?>app-assets/images/slider/03.jpg" alt="Card image">
                                <div class="card-img-overlay overlay-warning">
                                    <h4 class="card-title white mb-50">Overlay Card</h4>
                                    <p class="card-text text-ellipsis">
                                        Sugar plum tiramisu sweet. Cake jelly marshmallow cotton candy chupa
                                        chups carrot cake topping chupa chups.
                                    </p>
                                    <small>Last updated 3 mins ago</small>
                                </div>
                            </div>
                            <div class="card border-info text-center bg-transparent">
                                <div class="card-body">
                                    <div class="row">
                                        <div class="col-md-6 col-sm-12 mb-50 d-flex justify-content-center">
                                            <img src="<?= base_url('assets/'); ?>app-assets/images/elements/macbook-pro.png" alt="element 04" width="150" class="float-left mt-1 img-fluid">
                                        </div>
                                        <div class="col-md-6 col-sm-12 d-flex justify-content-center flex-column">
                                            <h4>
                                                <span class="badge badge-light-info">New Arrival</span>
                                            </h4>
                                            <p class="card-text">Mac Book.</p>
                                        </div>
                                    </div>
                                    <button class="btn btn-info mt-50">Buy Now</button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

        </div>
    </div>
</div>
</div>


<!-- Zero configuration table -->
<!-- <section id="basic-datatable">
     <div class="row">
         <div class="col-12">
             <div class="card">
                 <div class="card-header">
                     <h4 class="card-title"><?= $title; ?></h4>
                 </div>
                 <div class="card-body card-dashboard">
                     <a href="" class="btn btn-primary mb-1" data-toggle="modal" data-target="#newRoleModal">Add New Role</a>
                     <div class="table-responsive">

                         <?= form_error('menu', '<div class="alert alert-danger" role="alert">', '</div>'); ?>

                         <?= $this->session->flashdata('message'); ?>

                         <table class="table zero-configuration">
                             <thead>
                                 <tr>
                                     <th>#</th>
                                     <th>Nama</th>
                                     <th>Email</th>
                                     <th>Image</th>
                                     <th>Date Created</th>
                                 </tr>
                             </thead>
                             <tbody>
                                 <?php $i = 1; ?>
                                 <?php foreach ($test as $r) : ?>
                                     <tr>
                                         <th scope="row"><?= $i; ?></th>
                                         <td><?= $r['name']; ?></td>
                                         <td><?= $r['email']; ?></td>
                                         <td><?= $r['image']; ?></td>
                                         <td><?= date("d/m/Y H:i:s", $r['date_created']); ?></td>
                                     </tr>
                                     <?php $i++; ?>
                                 <?php endforeach; ?>
                             </tbody>
                         </table>
                     </div>
                 </div>
             </div>
         </div>
     </div>
 </section> -->
<!--/ Zero configuration table -->

<script>
    var $primary = '#5A8DEE';
    var $success = '#39DA8A';
    var $danger = '#FF5B5C';
    var $warning = '#FDAC41';
    var $info = '#00CFDD';
    var $label_color = '#475f7b';
    var $primary_light = '#E2ECFF';
    var $danger_light = '#ffeed9';
    var $gray_light = '#828D99';
    var $sub_label_color = "#596778";
    var $radial_bg = "#e7edf3";


    var xValues = ["Italy", "France", "Spain", "USA", "Argentina"];
    var yValues = [55, 49, 44, 24, 15];
    var barColors = ["red", "green", "blue", "orange", "brown"];

    new Chart("myChart", {
        type: "bar",
        data: {
            labels: xValues,
            datasets: [{
                backgroundColor: barColors,
                data: yValues
            }]
        },
        options: {
            legend: {
                display: false
            },
            title: {
                display: true,
                text: "World Wine Production 2018"
            }
        }
    });

    const ctx = document.getElementById('myChartx');
    const myChartx = new Chart(ctx, {
        type: 'bar',
        data: {
            labels: ['Red', 'Blue', 'Yellow', 'Green', 'Purple', 'Orange'],
            datasets: [{
                label: '# of Votes',
                data: [120012021, 19219380, 312798123, 517236721, 27864534, 397324672],
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                y: {
                    beginAtZero: true
                }
            }
        }
    });


    var options = {
        series: [{
            name: "Desktops",
            data: [10, 41, 35, 51, 49, 62, 69, 91, 148]
        }],
        chart: {
            height: 350,
            type: 'line',
            zoom: {
                enabled: false
            }
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'straight'
        },
        title: {
            text: 'Product Trends by Month',
            align: 'left'
        },
        grid: {
            row: {
                colors: ['#f3f3f3', 'transparent'], // takes an array which will be repeated on columns
                opacity: 0.5
            },
        },
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep'],
        }
    };

    var chart = new ApexCharts(document.querySelector("#myChartz"), options);
    chart.render();


    var options = {
        series: [{
            name: 'series1',
            data: [31, 40, 28, 51, 42, 109, 100]
        }, {
            name: 'series2',
            data: [11, 32, 45, 32, 34, 52, 41]
        }],
        chart: {
            height: 350,
            type: 'area'
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            curve: 'smooth'
        },
        xaxis: {
            type: 'datetime',
            categories: ["2018-09-19T00:00:00.000Z", "2018-09-19T01:30:00.000Z", "2018-09-19T02:30:00.000Z", "2018-09-19T03:30:00.000Z", "2018-09-19T04:30:00.000Z", "2018-09-19T05:30:00.000Z", "2018-09-19T06:30:00.000Z"]
        },
        tooltip: {
            x: {
                format: 'dd/MM/yy HH:mm'
            },
        },
    };

    var chart = new ApexCharts(document.querySelector("#chartArea"), options);
    chart.render();


    var options = {
        series: [{
            name: 'Net Profit',
            data: [44, 55, 57, 56, 61, 58, 63, 60, 66, 0, 40, 70]
        }, {
            name: 'Revenue',
            data: [76, 85, 101, 98, 87, 105, 91, 114, 94, 80, 0, 30]
        }, {
            name: 'Free Cash Flow',
            data: [35, 41, 36, 26, 45, 48, 52, 53, 41, 50, 89, 0]
        }],
        chart: {
            type: 'bar',
            height: 350
        },
        plotOptions: {
            bar: {
                horizontal: false,
                columnWidth: '55%',
                endingShape: 'rounded'
            },
        },
        dataLabels: {
            enabled: false
        },
        stroke: {
            show: true,
            width: 2,
            colors: ['transparent']
        },
        xaxis: {
            categories: ['Jan', 'Feb', 'Mar', 'Apr', 'May', 'Jun', 'Jul', 'Aug', 'Sep', 'Oct', 'Nov', 'Des'],
        },
        yaxis: {
            title: {
                text: '$ (thousands)'
            }
        },
        fill: {
            opacity: 1
        },
        tooltip: {
            y: {
                formatter: function(val) {
                    return "$ " + val + " thousands"
                }
            }
        }
    };

    var chart = new ApexCharts(document.querySelector("#myColumnChart"), options);
    chart.render();



    var options = {
        series: [100],
        chart: {
            height: 350,
            type: 'radialBar',
            offsetY: -10
        },
        plotOptions: {
            radialBar: {
                startAngle: -135,
                endAngle: 135,
                dataLabels: {
                    name: {
                        fontSize: '16px',
                        color: undefined,
                        offsetY: 120
                    },
                    value: {
                        offsetY: 76,
                        fontSize: '22px',
                        color: undefined,
                        formatter: function(val) {
                            return val + "%";
                        }
                    }
                }
            }
        },
        // fill: {
        //     type: 'gradient',
        //     gradient: {
        //         shade: 'dark',
        //         shadeIntensity: 0.15,
        //         inverseColors: false,
        //         opacityFrom: 1,
        //         opacityTo: 1,
        //         stops: [0, 50, 65, 91]
        //     },
        // },
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: 'horizontal',
                shadeIntensity: 0.5,
                gradientToColors: [$success],
                inverseColors: false,
                opacityFrom: 1,
                opacityTo: 1,
                stops: [0, 100]
            },
        },
        stroke: {
            dashArray: 4
        },
        labels: ['Median Ratio'],
    };

    var chart = new ApexCharts(document.querySelector("#strokedChart"), options);
    chart.render();



    // Primary Line Chart
    // -----------------------------
    var primaryLineChartOption = {
        chart: {
            height: 40,
            // width: 180,
            type: 'line',
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true,
            },
        },
        grid: {
            show: false,
            padding: {
                bottom: 5,
                top: 5,
                left: 10,
                right: 0
            }
        },
        colors: [$primary],
        dataLabels: {
            enabled: false,
        },
        stroke: {
            width: 3,
            curve: 'smooth'
        },
        series: [{
            data: [50, 100, 0, 60, 20, 30]
        }],
        fill: {
            type: 'gradient',
            gradient: {
                shade: 'dark',
                type: "horizontal",
                gradientToColors: [$primary],
                opacityFrom: 0,
                opacityTo: 0.9,
                stops: [0, 30, 70, 100]
            }
        },
        xaxis: {
            show: false,
            labels: {
                show: false
            },
            axisBorder: {
                show: false
            }
        },
        yaxis: {
            show: false
        },
    }


    //  primary line chart
    var primaryLineChart = new ApexCharts(
        document.querySelector("#primary-line-chart"),
        primaryLineChartOption
    );
    primaryLineChart.render();


    // Stacked Bar Nagetive Chart
    // ----------------------------------
    var barNegativeChartoptions = {
        chart: {
            height: 110,
            stacked: true,
            type: 'bar',
            toolbar: {
                show: false
            },
            sparkline: {
                enabled: true,
            },
        },
        plotOptions: {
            bar: {
                columnWidth: '20%',
                endingShape: 'rounded',
            },
            distributed: true,
        },
        colors: [$primary, $warning],
        series: [{
            name: 'New Clients',
            data: [75, 150, 225, 200, 35, 50, 150, 180, 50, 150, 240, 140, 75, 35, 60, 120]
        }, {
            name: 'Retained Clients',
            data: [-100, -55, -40, -120, -70, -40, -60, -50, -70, -30, -60, -40, -50, -70, -40, -50],
        }],
        grid: {
            show: false,
        },
        legend: {
            show: false,
        },
        dataLabels: {
            enabled: false
        },
        tooltip: {
            x: {
                show: false
            }
        },
    }

    var barNegativeChart = new ApexCharts(
        document.querySelector("#bar-negative-chart"),
        barNegativeChartoptions
    );
</script>