<html>

<head>

    <link rel="stylesheet" href="styles.css">
    <!-- <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/css/bootstrap.min.css">
    </link>
    <link href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.0/js/bootstrap.min.js">
    </link>
    <link href="https://use.fontawesome.com/releases/v5.8.1/css/all.css">
    </link> -->
</head>

<body>
    <div class="container mt-5 mb-3">
        <div class="row d-flex justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="d-flex flex-row p-2">
                        <!-- <img src="<?= base_url('assets/'); ?>app-assets/images/flags/de.png" width="48"> -->
                        <img src="<?= base_url('assets/'); ?>img/dankos_logo.jpg" width="75">
                        <!-- <div class="d-flex flex-column text-center">Formulir
                            <?= $receive_data['jenis_form'] == 'receive' ? 'Penerimaan<s>/Pengiriman</s>' : '<s>Penerimaan/</s>Pengiriman' ?>
                            Obat Jadi</div> -->
                        <br>
                        <br>
                        <div class="d-flex flex-column text-center" style="text-align: center">Formulir Penerimaan/Pengiriman Obat Jadi</div>
                    </div>
                    <hr>
                    <br>
                    <br>
                    <div class="table-responsive p-2">
                        <table class="table table-borderless" style="margin-top: -15px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content" style="display: flex;">
                                    <!-- <div class="justify-left"> -->
                                    <td class="" style="font-size: 12px;">
                                            Tanggal : <?= date('d-m-Y', strtotime($receive_data['tanggal'])); ?>
                                            <br>
                                            Dist/Cust : <?= $receive_data['distributor_customer'] ?>
                                            <br>
                                            Ekspedisi : <?= $receive_data['ekspedisi'] ?>
                                            <br>
                                            No polisi : <?= $receive_data['no_polisi'] ?>
                                    </td>
                                    <td class="" style="font-size: 12px; padding-left: 150px;">

                                            Suhu awal : <?= $receive_data['suhu_awal'] ?>
                                            <br>
                                            Suhu setelah : <?= $receive_data['suhu_setelah'] ?>
                                            <br>
                                            Jam awal : <?= $receive_data['jam_awal'] ?>
                                            <br>
                                            Jam setelah : <?= $receive_data['jam_setelah'] ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <hr>
                    <br>
                    <div class="d-flex flex-column text-center text-bold-600" style="text-align: center">
                        <?= $receive_data['jenis_form'] == 'receive' ? 'Penerimaan<s>/Pengiriman</s>' : '<s>Penerimaan/</s>Pengiriman' ?>
                        * </div>
                    <hr>
                    <br>
                    <div class="products p-2" style="margin-bottom: 10px;">
                        <table class="table table-borderless" style="margin-top: -10px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content">
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|No| </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Batch Number| </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|LPN Number| </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Qty MB| </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Qty / MB| </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Qty Pick| </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Product Check Out| </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Qty EPM Pallet| </td>
                                    <!-- <td class="text-center">Total</td> -->
                                </tr>
                                <?php $i = 1;
                                $total = 0 ?>
                                <?php foreach ($rd_data as $r) : ?>
                                <tr class="">
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $i ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $r['batch_number']; ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $r['lpn_number']; ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $r['qty_mb']; ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $r['qty_per_mb']; ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $r['qty_pick']; ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $r['product_checklist_out']; ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center"><?= $r['qty_epm_pallet']; ?>
                                    </td>
                                    </tr>
                                    <?php $i++;
                                    $total = $total + $r['qty_epm_pallet']; ?>
                                <?php endforeach; ?>
                            </tbody>
                        </table>
                    </div>
                    <!--<br>-->
                    <hr>
                    <div class="d-flex flex-column text-right" style="margin-top: -5px; margin-bottom: -5px; margin-right: 45px; font-size: 11px; text-align: right"> Total : <?= $total; ?> 
                    </div>
                    <hr>
                    <div class="d-flex flex-column text-left" style="margin-top: -10px; margin-bottom: -10px; font-size: 12;">
                        Catatan : <br><small style="font-size: 8; color:red;">(catat pada kolom ini jika terdapat produk kondisi basah/rusak dalam bentuk apapun atau terjadi penyimpangan):</small>
                        <br>
                        <?= $receive_data['catatan'] ?>
                        <br><br>
                        <small style="font-size: 10;">
                            No seal/segel:
                            <!-- <br> -->
                            <?= $receive_data['segel'] ?>
                        </small>
                    </div>
                    <hr>
                    <div class="d-flex flex-column text-center text-bold-600" style="margin-top: 0px; margin-bottom: 0px; text-align: center;">Kondisi Kendaraan</div>
                    <hr>
                    <div class="products p-2" style="margin-bottom: 10px;">
                        <table class="table table-borderless" style="margin-top: -10px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content justify-content-start">
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Kebersihan| 
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Bunyi Mesin| 
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Asap Kendaraan| 
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Bau| 
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Kebocoran| 
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Uji Emisi/KIR| 
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 5px; padding-right: 5px;" class="text-center">|Tetesan Oli| 
                                    </td>
                                </tr>
                                <br>
                                <tr class="">
                                    <td style="font-size: 11; text-align: center; padding-left: 10px; padding-right: 10px;" class="text-center">
                                        <?= $receive_data['kebersihan_kendaraan'] ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 10px; padding-right: 10px;" class="text-center">
                                        <?= $receive_data['bunyi_mesin_kendaraan'] ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 10px; padding-right: 10px;" class="text-center">
                                        <?= $receive_data['asap_kendaraan'] ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 10px; padding-right: 10px;" class="text-center">
                                        <?= $receive_data['bau_kendaraan'] ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 10px; padding-right: 10px;" class="text-center">
                                        <?= $receive_data['kebocoran_kendaraan'] ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 10px; padding-right: 10px;" class="text-center">
                                        <?= $receive_data['emisi_kendaraan'] ?>
                                    </td>
                                    <td style="font-size: 11; text-align: center; padding-left: 10px; padding-right: 10px;" class="text-center">
                                        <?= $receive_data['oli_kendaraan'] ?>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <hr>
                    <div style="margin-bottom: 10px;"></div>
                    <div class="table-responsive p-2">
                        <table class="table table-borderless" style="margin-top: -15px; margin-bottom: -15px;">
                            <tbody>
                                <tr class="content">
                                    <td class="">
                                        <small>Keterangan</small>
                                        <ul>
                                            <li>
                                                <small>
                                                    Pencatatan dilakukan untuk produk dengan <br>
                                                    temperatur khusus (2-8ºC atau &lt;25ºC)
                                                </small>
                                            </li>
                                            <li>
                                                <small>
                                                    C atau ȼ adalah koli
                                                </small>
                                            </li>
                                            <li>
                                                <small>
                                                    Jumlah palet EPM hanya untuk pengiriman obat jadi
                                                </small>
                                                <ul>
                                                    <li><small>coret yang tidak sesuai *</small></li>
                                                    <li><small>catat nomor seal/segel pada kolom CATATAN **</small></li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </td>
                                    <td class="" >
                                    <p style="font-size: 12px;">Dikerjakan oleh</p> 
                                    <br>
                                        <table class="table table-borderless">
                                            <tbody>
                                                <tr class="content justify-content-start">
                                                    <td class="text-center">
                                                        <img style="width: 100px; height: 100px;" src="<?= base_url('assets/img/profile/') . $receive_data['image'] ?>" class="img-thumbnail">
                                                    </td>
                                                    <td class="text-center">
                                                        <img style="width: 100px; height: 100px;" src="<?= base_url('assets/img/foto_ekspedisi/') . $receive_data['foto_ekspedisi']; ?>" class="img-thumbnail">
                                                    </td>
                                                </tr>
                                                <br>
                                                <br>
                                                <tr class="">
                                                    <td class="text-center" style="font-size: 10px;"><?= $receive_data['name'] ?></td>
                                                    <td class="text-center" style="font-size: 10px;"><?= $receive_data['nama_ekspedisi'] ?></td>
                                            </tbody>
                                        </table>
                                    </td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                    <br>
                    <div class="d-flex flex-column text-center">
                        <small class="text-center" style="font-size: 10; color:red; text-align: 'center';">
                            Dokumen ini sebagai bukti valid produk/barang yang FG PT Dankos Farma terima atau shipping secara jumlah, nomor batch, kondisi
                            dan keterangan lainnya yang tertera dan ditandatangani oleh pihak ekspedisi.
                        </small>
                    </div>


                    <!-- <div class="products p-2">
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="add">
                                    <td></td>
                                    <td>Subtotal</td>
                                    <td>GST(10%)</td>
                                    <td class="text-center">Total</td>
                                </tr>
                                <tr class="content">
                                    <td></td>
                                    <td>$40,000</td>
                                    <td>2,500</td>
                                    <td class="text-center">$42,500</td>
                                </tr>
                            </tbody>
                        </table>
                    </div> -->
                    <!-- <hr>
                    <div class="address p-2">
                        <table class="table table-borderless">
                            <tbody>
                                <tr class="add">
                                    <td>Bank Details</td>
                                </tr>
                                <tr class="content">
                                    <td> Bank Name : ADS BANK <br> Swift Code : ADS1234Q <br> Account Holder : Jelly Pepper <br> Account Number : 5454542WQR <br> </td>
                                </tr>
                            </tbody>
                        </table>
                    </div> -->
                </div>
            </div>
        </div>
    </div>

</html>