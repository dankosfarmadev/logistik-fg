<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Admin_model extends CI_Model
{
    public function getWorker()
    {
        $query = "SELECT `user`.*, `departement`.`departement`
                  FROM `user` JOIN `departement`
                  ON `user`.`departement_id` = `departement`.`id`
                  WHERE `user`.`role_id` != 1
                ";
        return $this->db->query($query)->result_array();
    }

    public function getSpecificWorker($id)
    {
        // $query = "SELECT `user`.*, `departement`.`departement`
        //           FROM `user` JOIN `departement`
        //           ON `user`.`departement_id` = `departement`.`id`
        //           WHERE `user`.`id` = $id
        //         ";
        // return $this->db->query($query)->result_array();


        $this->db->select('user.*, departement.departement');
        $this->db->from('user');
        $this->db->join('departement', 'departement.id = user.departement_id');
        $this->db->where('user.id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->result_array();
    }

    public function getAllDept()
    {
        return $this->db->get('departement')->result_array();
    }

    public function getAllRole()
    {
        $this->db->select('*');
        $this->db->from('user_role');
        $this->db->where('user_role.id !=', 1); // Produces: WHERE name = 'Joe'
        return $this->db->get()->result_array();
    }

    public function workerUpdate($id, $data)
    {
        $this->db->where('id', $id);
        $this->db->update('user', $data);
    }
}
