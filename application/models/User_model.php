<?php
defined('BASEPATH') or exit('No direct script access allowed');

class User_model extends CI_Model
{
    public function getUser($nik)
    {
        $query = "SELECT `user`.*, `departement`.`departement`
                  FROM `user` JOIN `departement`
                  ON `user`.`departement_id` = `departement`.`id`
                  WHERE `user`.`nik` = $nik
                ";
        return $this->db->query($query)->result_array();
    }
}
