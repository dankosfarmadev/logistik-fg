<?php
defined('BASEPATH') or exit('No direct script access allowed');

class Erp_model extends CI_Model
{
    public function getAllWhere()
    {
        $this->db->select('receive_delivery.*, master_status_form_rd.status, user.name');
        $this->db->from('receive_delivery');
        $this->db->join('master_status_form_rd', 'receive_delivery.status_form_rd_id = master_status_form_rd.id');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->where('receive_delivery.status_form_rd_id', 4);
        $this->db->where('receive_delivery.jenis_form != ', 'delivery_export');
        $this->db->order_by('receive_delivery.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }

    public function getAllWhereExport()
    {
        $this->db->select('receive_delivery.*, master_status_form_rd.status, user.name');
        $this->db->from('receive_delivery');
        $this->db->join('master_status_form_rd', 'receive_delivery.status_form_rd_id = master_status_form_rd.id');
        $this->db->join('user', 'receive_delivery.create_by = user.id');
        $this->db->where('receive_delivery.status_form_rd_id', 4);
        $this->db->where('receive_delivery.jenis_form = ', 'delivery_export');
        $this->db->order_by('receive_delivery.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }


    public function getAllWhereDoc()
    {
        $this->db->select('inspection_checklist.*, master_status_form_inspection.status, user.name');
        $this->db->from('inspection_checklist');
        $this->db->join('master_status_form_inspection', 'inspection_checklist.status_form_inspection_id = master_status_form_inspection.id');
        $this->db->join('user', 'inspection_checklist.create_by = user.id');
        $this->db->where('inspection_checklist.status_form_inspection_id', 4);
        $this->db->order_by('inspection_checklist.id', 'DESC');
        // $this->db->where('receive_delivery.create_by', $this->session->userdata('id'));
        return $query = $this->db->get()->result_array();
    }

    public function getSpecificInspectionChecklist($id)
    {
        $this->db->select('inspection_checklist.*');
        $this->db->from('inspection_checklist');
        $this->db->where('id', $id); // Produces: WHERE name = 'Joe'
        return $query = $this->db->get()->row_array();
    }
}
