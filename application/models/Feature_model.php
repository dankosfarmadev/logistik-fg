

    <?php
    defined('BASEPATH') or exit('No direct script access allowed');

    class Feature_model extends CI_Model
    {
        public function do_log($query)
        {
            $data = [
                'user_id' => $this->session->userdata('id'),
                'name' => $this->session->userdata('name'),
                'log_describe' => $query
            ];

            $this->db->insert('log', $data);
        }


        public function pull_log()
        {
            $query = "SELECT * FROM log WHERE DATE(date) > (NOW() - INTERVAL 7 DAY) order by id desc";
            return $this->db->query($query)->result_array();
        }
    }
