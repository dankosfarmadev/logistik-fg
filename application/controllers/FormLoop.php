 
<?php

defined('BASEPATH') or exit('No direct script access allowed');

class FormLoop extends CI_Controller
{

  public function index()
  {
    $data['title'] = "Insert Form Looping Using Javascript";
    $this->load->view('form/form_loop', $data);
  }

  public function post()
  {
    $i = 0; // untuk loopingnya
    $a = $this->input->post('first_name');
    $b = $this->input->post('last_name');

    $x = $this->input->post('lpn');

    // var_dump($this->input->post('last_name'));
    echo "<pre>";
    print_r($_POST);
    // print_r($_POST['first_name'][0]);
    // print_r($_POST['last_name'][0]);

    // print_r(print_r($_POST['first_name'][0]));
    // print_r(print_r($_POST['last_name'][0]));

    echo "</pre>";


    // if ($a[0] !== null) {
    //   foreach ($a as $row) {
    //     $data = [
    //       'first_name' => $row,
    //       'last_name' => $b[$i],
    //     ];

    //     $insert = $this->db->insert('biodata', $data);
    //     if ($insert) {
    //       $i++;
    //     }
    //   }
    // }

    // $arr['success'] = true;
    // $arr['notif']  = '<div class="alert alert-success">
    //   <i class="fa fa-check"></i> Data Berhasil Disimpan
    // </div>';
    // return $this->output->set_output(json_encode($arr));
  }
}
