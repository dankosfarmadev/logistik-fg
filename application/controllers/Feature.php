<?php
defined('BASEPATH') or exit('No direct script access allowed');

use PhpOffice\PhpSpreadsheet\Spreadsheet;
use PhpOffice\PhpSpreadsheet\Writer\Xlsx;

use PhpOffice\PhpSpreadsheet\Helper\Sample;
use PhpOffice\PhpSpreadsheet\IOFactory;

// require FCPATH . 'vendor/autoload.php';
// require(APPPATH . 'vendor/autoload.php');


class Feature extends CI_Controller
{
    public function __construct()
    {
        parent::__construct();
        $this->load->library('form_validation');
        // is_logged_in();

        // Load semua model disini
        $this->load->model('Menu_model');

        $this->load->library('form_validation');
        $this->load->library('image_lib');

        $this->load->helper('cookie');
        $this->load->model('Worker_model');
    }

    // tampilan beserta aturan segment
    public function index()
    {
    }

    public function testSendMail()
    {
        $this->Mailer_model->sendMail('Dankos Farma Dev by MSTD', 'ndusetwn@gmail.com', 'ndusetwn16@gmail.com, pandu.setiawan@dankosfarma.com', 'verify', '[Notification]', 'Message Email', 'admin');
    }

    public function receiveXlsx()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="receive.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Jenis form');
        $sheet->setCellValue('B1', 'Tanggal');
        $sheet->setCellValue('C1', 'Distributor/Customer');
        $sheet->setCellValue('D1', 'Ekspedisi');
        $sheet->setCellValue('E1', 'No pollisi');
        $sheet->setCellValue('F1', 'Suhu awal (Celcius)');
        $sheet->setCellValue('G1', 'Suhu setelah (Celcius)');
        $sheet->setCellValue('H1', 'Jam awal');
        $sheet->setCellValue('I1', 'Jam setelah');
        $sheet->setCellValue('J1', 'Status Dokumen');

        $sheet->setCellValue('K1', 'Kebersihan');
        $sheet->setCellValue('L1', 'Bunyi mesin');
        $sheet->setCellValue('M1', 'Asap');
        $sheet->setCellValue('N1', 'Bau');
        $sheet->setCellValue('O1', 'Kebocoran');

        $sheet->setCellValue('P1', 'Nama Pembuat');

        $sheet->setCellValue('Q1', 'Create date');
        $sheet->setCellValue('R1', 'Update date');



        // // Fetch data
        $row = 2;
        $wkwk['test'] = $this->Worker_model->getReceive();


        foreach ($wkwk['test'] as $key) {
            // $key['is_active'] = $key['is_active'] == 1 ? 'Active' : 'Not Active';
            $sheet->setCellValue('A' . $row, $key['jenis_form']);
            $sheet->setCellValue('B' . $row, $key['tanggal']);
            $sheet->setCellValue('C' . $row, $key['distributor_customer']);
            $sheet->setCellValue('D' . $row, $key['ekspedisi']);
            $sheet->setCellValue('E' . $row, $key['no_polisi']);
            $sheet->setCellValue('F' . $row, $key['suhu_awal']);
            $sheet->setCellValue('G' . $row, $key['suhu_setelah']);
            $sheet->setCellValue('H' . $row, $key['jam_awal']);
            $sheet->setCellValue('I' . $row, $key['jam_setelah']);
            $sheet->setCellValue('J' . $row, $key['status']);
            $sheet->setCellValue('K' . $row, $key['kebersihan_kendaraan']);
            $sheet->setCellValue('L' . $row, $key['bunyi_mesin_kendaraan']);
            $sheet->setCellValue('M' . $row, $key['asap_kendaraan']);
            $sheet->setCellValue('N' . $row, $key['bau_kendaraan']);
            $sheet->setCellValue('O' . $row, $key['kebocoran_kendaraan']);
            $sheet->setCellValue('P' . $row, $key['name']);
            $sheet->setCellValue('Q' . $row, $key['create_date']);
            $sheet->setCellValue('R' . $row, $key['update_date']);

            // $sheet->setCellValue('Q' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            // echo $key['title'];
            // echo $key['menu'];
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }

    public function deliveryXlsx()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="delivery.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Jenis form');
        $sheet->setCellValue('B1', 'Tanggal');
        $sheet->setCellValue('C1', 'Distributor/Customer');
        $sheet->setCellValue('D1', 'Ekspedisi');
        $sheet->setCellValue('E1', 'No pollisi');
        $sheet->setCellValue('F1', 'Suhu awal (Celcius)');
        $sheet->setCellValue('G1', 'Suhu setelah (Celcius)');
        $sheet->setCellValue('H1', 'Jam awal');
        $sheet->setCellValue('I1', 'Jam setelah');
        $sheet->setCellValue('J1', 'Status Dokumen');

        $sheet->setCellValue('K1', 'Kebersihan');
        $sheet->setCellValue('L1', 'Bunyi mesin');
        $sheet->setCellValue('M1', 'Asap');
        $sheet->setCellValue('N1', 'Bau');
        $sheet->setCellValue('O1', 'Kebocoran');

        $sheet->setCellValue('P1', 'Nama Pembuat');

        $sheet->setCellValue('Q1', 'Create date');
        $sheet->setCellValue('R1', 'Update date');



        // // Fetch data
        $row = 2;
        $wkwk['test'] = $this->Worker_model->getDelivery();


        foreach ($wkwk['test'] as $key) {
            // $key['is_active'] = $key['is_active'] == 1 ? 'Active' : 'Not Active';
            $sheet->setCellValue('A' . $row, $key['jenis_form']);
            $sheet->setCellValue('B' . $row, $key['tanggal']);
            $sheet->setCellValue('C' . $row, $key['distributor_customer']);
            $sheet->setCellValue('D' . $row, $key['ekspedisi']);
            $sheet->setCellValue('E' . $row, $key['no_polisi']);
            $sheet->setCellValue('F' . $row, $key['suhu_awal']);
            $sheet->setCellValue('G' . $row, $key['suhu_setelah']);
            $sheet->setCellValue('H' . $row, $key['jam_awal']);
            $sheet->setCellValue('I' . $row, $key['jam_setelah']);
            $sheet->setCellValue('J' . $row, $key['status']);
            $sheet->setCellValue('K' . $row, $key['kebersihan_kendaraan']);
            $sheet->setCellValue('L' . $row, $key['bunyi_mesin_kendaraan']);
            $sheet->setCellValue('M' . $row, $key['asap_kendaraan']);
            $sheet->setCellValue('N' . $row, $key['bau_kendaraan']);
            $sheet->setCellValue('O' . $row, $key['kebocoran_kendaraan']);
            $sheet->setCellValue('P' . $row, $key['name']);
            $sheet->setCellValue('Q' . $row, $key['create_date']);
            $sheet->setCellValue('R' . $row, $key['update_date']);

            // $sheet->setCellValue('Q' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            // echo $key['title'];
            // echo $key['menu'];
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }

    public function deliveryExportXlsx()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="deliveryExport.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Jenis form');
        $sheet->setCellValue('B1', 'Tanggal');
        $sheet->setCellValue('C1', 'Distributor/Customer');
        $sheet->setCellValue('D1', 'Ekspedisi');
        $sheet->setCellValue('E1', 'No pollisi');
        $sheet->setCellValue('F1', 'Suhu awal (Celcius)');
        $sheet->setCellValue('G1', 'Suhu setelah (Celcius)');
        $sheet->setCellValue('H1', 'Jam awal');
        $sheet->setCellValue('I1', 'Jam setelah');
        $sheet->setCellValue('J1', 'Status Dokumen');

        $sheet->setCellValue('K1', 'Kebersihan');
        $sheet->setCellValue('L1', 'Bunyi mesin');
        $sheet->setCellValue('M1', 'Asap');
        $sheet->setCellValue('N1', 'Bau');
        $sheet->setCellValue('O1', 'Kebocoran');

        $sheet->setCellValue('P1', 'Nama Pembuat');

        $sheet->setCellValue('Q1', 'Create date');
        $sheet->setCellValue('R1', 'Update date');



        // // Fetch data
        $row = 2;
        $wkwk['test'] = $this->Worker_model->getDeliveryExport();


        foreach ($wkwk['test'] as $key) {
            // $key['is_active'] = $key['is_active'] == 1 ? 'Active' : 'Not Active';
            $sheet->setCellValue('A' . $row, $key['jenis_form']);
            $sheet->setCellValue('B' . $row, $key['tanggal']);
            $sheet->setCellValue('C' . $row, $key['distributor_customer']);
            $sheet->setCellValue('D' . $row, $key['ekspedisi']);
            $sheet->setCellValue('E' . $row, $key['no_polisi']);
            $sheet->setCellValue('F' . $row, $key['suhu_awal']);
            $sheet->setCellValue('G' . $row, $key['suhu_setelah']);
            $sheet->setCellValue('H' . $row, $key['jam_awal']);
            $sheet->setCellValue('I' . $row, $key['jam_setelah']);
            $sheet->setCellValue('J' . $row, $key['status']);
            $sheet->setCellValue('K' . $row, $key['kebersihan_kendaraan']);
            $sheet->setCellValue('L' . $row, $key['bunyi_mesin_kendaraan']);
            $sheet->setCellValue('M' . $row, $key['asap_kendaraan']);
            $sheet->setCellValue('N' . $row, $key['bau_kendaraan']);
            $sheet->setCellValue('O' . $row, $key['kebocoran_kendaraan']);
            $sheet->setCellValue('P' . $row, $key['name']);
            $sheet->setCellValue('Q' . $row, $key['create_date']);
            $sheet->setCellValue('R' . $row, $key['update_date']);

            // $sheet->setCellValue('Q' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            // echo $key['title'];
            // echo $key['menu'];
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }

    public function inspectionChecklistXlsx()
    {
        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="inspection_checklist.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Nama pembuat');
        $sheet->setCellValue('B1', 'Status dokumen');
        $sheet->setCellValue('C1', 'Create date');
        $sheet->setCellValue('D1', 'No polisi');
        $sheet->setCellValue('E1', 'Catatan');
        $sheet->setCellValue('F1', '1a');
        $sheet->setCellValue('G1', '1b');
        $sheet->setCellValue('H1', '1c');
        $sheet->setCellValue('I1', '2a');
        $sheet->setCellValue('J1', '2b');
        $sheet->setCellValue('K1', '2c');
        $sheet->setCellValue('L1', '3a');
        $sheet->setCellValue('M1', '3b');
        $sheet->setCellValue('N1', '4a');
        $sheet->setCellValue('O1', '4b');
        $sheet->setCellValue('P1', '5a');
        $sheet->setCellValue('Q1', '5b');
        $sheet->setCellValue('R1', '5c');
        $sheet->setCellValue('S1', '6a');
        $sheet->setCellValue('T1', '6b');
        $sheet->setCellValue('U1', '6c');
        $sheet->setCellValue('V1', '7a');
        $sheet->setCellValue('W1', '7b');
        $sheet->setCellValue('X1', '7c');


        // // Fetch data
        $row = 2;
        $wkwk['test'] = $this->Worker_model->getInspection();


        foreach ($wkwk['test'] as $key) {
            // $key['is_active'] = $key['is_active'] == 1 ? 'Active' : 'Not Active';
            $sheet->setCellValue('A' . $row, $key['name']);
            $sheet->setCellValue('B' . $row, $key['status']);
            $sheet->setCellValue('C' . $row, $key['create_date']);
            $sheet->setCellValue('D' . $row, $key['no_polisi']);
            $sheet->setCellValue('E' . $row, $key['catatan']);

            $sheet->setCellValue('F' . $row, $key['1a']);
            $sheet->setCellValue('G' . $row, $key['1b']);
            $sheet->setCellValue('H' . $row, $key['1c']);
            $sheet->setCellValue('I' . $row, $key['2a']);
            $sheet->setCellValue('J' . $row, $key['2b']);
            $sheet->setCellValue('K' . $row, $key['2c']);
            $sheet->setCellValue('L' . $row, $key['3a']);
            $sheet->setCellValue('M' . $row, $key['3b']);
            $sheet->setCellValue('N' . $row, $key['4a']);
            $sheet->setCellValue('O' . $row, $key['4b']);
            $sheet->setCellValue('P' . $row, $key['5a']);
            $sheet->setCellValue('Q' . $row, $key['5b']);
            $sheet->setCellValue('R' . $row, $key['5c']);

            $sheet->setCellValue('S' . $row, $key['6a']);
            $sheet->setCellValue('T' . $row, $key['6b']);
            $sheet->setCellValue('U' . $row, $key['6c']);

            $sheet->setCellValue('V' . $row, $key['7a']);
            $sheet->setCellValue('W' . $row, $key['7b']);
            $sheet->setCellValue('X' . $row, $key['7c']);
            // $sheet->setCellValue('Q' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            // echo $key['title'];
            // echo $key['menu'];
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }


    public function submenuXlsx()
    {

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="submenu.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Title');
        $sheet->setCellValue('B1', 'Menu');
        $sheet->setCellValue('C1', 'URL');
        $sheet->setCellValue('D1', 'Icon');
        $sheet->setCellValue('E1', 'Active');
        $sheet->setCellValue('F1', 'Update Date');

        // // Fetch data
        $row = 2;
        $this->load->model('Menu_model', 'menu');
        $wkwk['test'] = $this->menu->getSubMenu();


        foreach ($wkwk['test'] as $key) {
            $key['is_active'] = $key['is_active'] == 1 ? 'Active' : 'Not Active';
            $sheet->setCellValue('A' . $row, $key['title']);
            $sheet->setCellValue('B' . $row, $key['menu']);
            $sheet->setCellValue('C' . $row, $key['url']);
            $sheet->setCellValue('D' . $row, $key['icon']);
            $sheet->setCellValue('E' . $row, $key['is_active']);
            $sheet->setCellValue('F' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            // echo $key['title'];
            // echo $key['menu'];
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }

    public function menuXlsx()
    {

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="menu.xlsx"');
        header('Cache-Control: max-age=0');
        $spreadsheet = new Spreadsheet();
        $sheet = $spreadsheet->getActiveSheet();
        $sheet->setCellValue('A1', 'Menu');
        $sheet->setCellValue('B1', 'Update Date');

        // // Fetch data
        $row = 2;
        $this->load->model('Menu_model', 'menu');
        $fetch['test'] = $this->menu->getMenu();


        foreach ($fetch['test'] as $key) {
            $sheet->setCellValue('A' . $row, $key['menu']);
            $sheet->setCellValue('B' . $row, date('d-m-Y H:i:s', strtotime($key['update_date'])));
            $row++;
            # code...
        }
        $writer = IOFactory::createWriter($spreadsheet, 'Xlsx');
        $writer = new Xlsx($spreadsheet);
        ob_end_clean();
        $writer->save('php://output');
        die;
        exit;
    }

    public function pdfGen()
    {
        // panggil library yang kita buat sebelumnya yang bernama pdfgenerator
        $this->load->library('Pdfgenerator');

        // title dari pdf
        $this->data['title_pdf'] = 'Laporan Penjualan Toko Kita';

        // filename dari pdf ketika didownload
        $file_pdf = 'laporan_penjualan_toko_kita';
        // setting paper
        $paper = 'A4';
        //orientasi paper potrait / landscape
        $orientation = "portrait";

        $html = $this->load->view('menu/submenu', $this->data, true);

        // run dompdf
        $this->pdfgenerator->generate($html, $file_pdf, $paper, $orientation);
    }

    public function laporan_pdf()
    {

        $data = array(
            "dataku" => array(
                "nama" => "Petani Kode",
                "url" => "http://petanikode.com"
            )
        );

        $this->load->library('pdf');

        $this->pdf->setPaper('A4', 'potrait');
        $this->pdf->filename = "laporan-petanikode.pdf";
        $this->pdf->load_view('menu/submenu', $data);
    }
}
