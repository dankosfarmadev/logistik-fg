-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 23 Mar 2022 pada 01.54
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_logistic_fg`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `departement`
--

CREATE TABLE `departement` (
  `id` int(11) NOT NULL,
  `departement` varchar(128) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `departement`
--

INSERT INTO `departement` (`id`, `departement`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'MSTD', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(2, 'HCO', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(3, 'PPIC', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(4, 'FA', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(5, 'Line 01 (NBL 2)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(6, 'Line 01 (NBL 3)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(7, 'Line 02 (NBL Injeksi 37)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(8, 'Line 03 (NBL 1)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(9, 'Line 04', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(10, 'Line 05', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(11, 'Line 07 (NBL Sterile 39)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(12, 'Logistik', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(13, 'Engineering', '2022-03-11 15:16:29', 0, '2022-03-15 10:42:20', 0),
(14, 'RND', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(15, 'QA', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(16, 'QS', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(17, 'QC', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(18, 'TS', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(19, 'Project', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `nik` varchar(128) NOT NULL,
  `departement_id` int(11) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`, `nik`, `departement_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(5, 'Admin', 'dankosfarmadev@gmail.com', 'user-07.jpg', '$2y$10$9jH0sVB5zx9ebN.YYLKDZu9/rIf9JPVQCBDM/cLCLOGyVHLMYSHI2', 1, 1, 1552120289, 'admin', 1, '2022-03-11 14:08:10', 0, '2022-03-22 13:34:14', 0),
(6, 'Login Coordinator', 'user@gmail.com', 'avatar-s-1.jpg', '$2y$10$VB74gDuBRghG3PweE0N/UePvu.aPHnIKdSrX1gYRu.4L7IsP43kAW', 2, 1, 1552285263, '1', 12, '2022-03-11 14:08:10', 0, '2022-03-22 15:44:54', 5),
(26, 'Testing Logistik Worker', '2@gmail.com', 'default.jpg', '$2y$10$9jH0sVB5zx9ebN.YYLKDZu9/rIf9JPVQCBDM/cLCLOGyVHLMYSHI2', 3, 1, 1647933555, '2', 12, '2022-03-22 14:19:15', 0, '2022-03-22 15:37:57', 5),
(27, 'ERP', '3@gmail.com', 'default.jpg', '$2y$10$9jH0sVB5zx9ebN.YYLKDZu9/rIf9JPVQCBDM/cLCLOGyVHLMYSHI2', 4, 1, 1647937803, '3', 12, '2022-03-22 15:30:03', 0, '2022-03-22 15:38:26', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 1, 1, '2022-03-11 15:12:33', 0, '2022-03-11 15:12:33', 0),
(7, 1, 3, '2022-03-11 15:12:33', 0, '2022-03-22 15:17:18', 0),
(8, 1, 2, '2022-03-11 15:12:33', 0, '2022-03-11 15:12:33', 0),
(14, 2, 2, '2022-03-11 15:12:33', 0, '2022-03-11 15:12:33', 0),
(15, 2, 5, '2022-03-22 14:06:53', 0, '2022-03-22 14:06:53', 0),
(16, 3, 2, '2022-03-22 14:07:07', 0, '2022-03-22 14:07:07', 0),
(17, 3, 6, '2022-03-22 14:07:09', 0, '2022-03-22 14:07:09', 0),
(19, 4, 7, '2022-03-22 14:07:20', 0, '2022-03-22 14:07:20', 0),
(20, 4, 2, '2022-03-22 16:05:30', 0, '2022-03-22 16:05:30', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Admin', '2022-03-02 13:35:52', 0, '2022-03-15 09:38:09', 0),
(2, 'User', '2015-03-05 13:35:52', 0, '2022-03-22 15:18:22', 0),
(3, 'Menu', '2016-03-03 13:35:52', 0, '2022-03-22 15:18:39', 0),
(5, 'Coordinator', '2322-03-22 14:05:57', 0, '2022-03-22 14:51:26', 0),
(6, 'Worker', '2322-03-22 14:06:12', 0, '2022-03-22 14:51:36', 0),
(7, 'ERP', '2322-03-22 14:06:24', 0, '2022-03-22 16:00:43', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Administrator', '2022-03-11 15:17:52', 0, '2022-03-14 14:11:17', 0),
(2, 'Logistic Coordinator', '2022-03-11 15:17:52', 0, '2022-03-22 13:55:53', 0),
(3, 'Logistic Worker', '2022-03-22 14:03:35', 0, '2022-03-22 14:03:35', 0),
(4, 'ERP Support', '2022-03-22 14:04:50', 5, '2022-03-22 14:04:50', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 1, 'Dashboard', 'admin', 'bx bxs-dashboard', 1, '2022-03-11 15:08:45', 0, '2022-03-14 11:33:25', 0),
(2, 2, 'My Profile', 'user', 'bx bx-user', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(3, 2, 'Edit Profile', 'user/edit', 'bx bx-group', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(4, 3, 'Menu Management', 'menu', 'bx bxs-collection', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(5, 3, 'Submenu Management', 'menu/submenu', 'bx bxs-copy', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(7, 1, 'Role', 'admin/role', 'bx bxs-user-detail', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(8, 2, 'Change Password', 'user/changepassword', 'bx bx-key', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(9, 1, 'Worker', 'admin/worker', 'bx bxs-user-check', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(10, 5, 'Dashboard Coordinator', 'coordinator/dashboard', ' bx bxs-dashboard', 1, '2022-03-22 14:28:52', 0, '2022-03-22 14:30:37', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `departement`
--
ALTER TABLE `departement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=28;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=21;

--
-- AUTO_INCREMENT untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=8;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=11;

--
-- AUTO_INCREMENT untuk tabel `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
