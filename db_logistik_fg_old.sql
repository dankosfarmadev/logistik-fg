-- phpMyAdmin SQL Dump
-- version 5.1.1
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Waktu pembuatan: 21 Mar 2022 pada 03.11
-- Versi server: 10.4.22-MariaDB
-- Versi PHP: 7.4.27

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `db_logistik_fg`
--

-- --------------------------------------------------------

--
-- Struktur dari tabel `departement`
--

CREATE TABLE `departement` (
  `id` int(11) NOT NULL,
  `departement` varchar(128) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Dumping data untuk tabel `departement`
--

INSERT INTO `departement` (`id`, `departement`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'MSTD', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(2, 'HCO', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(3, 'PPIC', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(4, 'FA', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(5, 'Line 01 (NBL 2)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(6, 'Line 01 (NBL 3)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(7, 'Line 02 (NBL Injeksi 37)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(8, 'Line 03 (NBL 1)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(9, 'Line 04', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(10, 'Line 05', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(11, 'Line 07 (NBL Sterile 39)', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(12, 'Logistik', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(13, 'Engineering', '2022-03-11 15:16:29', 0, '2022-03-15 10:42:20', 0),
(14, 'RND', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(15, 'QA', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(16, 'QS', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(17, 'QC', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(18, 'TS', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0),
(19, 'Project', '2022-03-11 15:16:29', 0, '2022-03-11 15:16:29', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user`
--

CREATE TABLE `user` (
  `id` int(11) NOT NULL,
  `name` varchar(128) NOT NULL,
  `email` varchar(128) NOT NULL,
  `image` varchar(128) NOT NULL,
  `password` varchar(256) NOT NULL,
  `role_id` int(11) NOT NULL,
  `is_active` int(1) NOT NULL,
  `date_created` int(11) NOT NULL,
  `nik` varchar(128) NOT NULL,
  `departement_id` int(11) NOT NULL DEFAULT 1,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user`
--

INSERT INTO `user` (`id`, `name`, `email`, `image`, `password`, `role_id`, `is_active`, `date_created`, `nik`, `departement_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(5, 'Admin', 'dankosfarmadev@gmail.com', 'user-07.jpg', '$2y$10$9jH0sVB5zx9ebN.YYLKDZu9/rIf9JPVQCBDM/cLCLOGyVHLMYSHI2', 1, 1, 1552120289, '11', 1, '2022-03-11 14:08:10', 0, '2022-03-11 14:08:10', 0),
(6, 'Doddy Ferdiansyah', 'doddy@gmail.com', 'avatar-s-1.jpg', '$2y$10$94o6cb1m7wvetOyDuJBJGe1NJC8G3ESQACvBWl8bGcSNMb/QkIK9O', 2, 1, 1552285263, '12', 12, '2022-03-11 14:08:10', 0, '2022-03-15 11:32:13', 5),
(11, 'Sandhika Galih', 'sandhikagalih@gmail.com', 'default.jpg', '$2y$10$9jH0sVB5zx9ebN.YYLKDZu9/rIf9JPVQCBDM/cLCLOGyVHLMYSHI2', 2, 1, 1553151354, '13', 1, '2022-03-11 14:08:10', 0, '2022-03-14 14:16:09', 0),
(12, 'Pandu Setiawan', 'pandoe@gmail.com', 'user-11.jpg', '$2y$10$ResDu79ZW0fQqhiVtCD1Duijah8Tqfh5uCueZstroGpBzx5TXR642', 2, 1, 1646876469, '14', 1, '2022-03-11 14:08:10', 0, '2022-03-14 14:16:13', 0),
(13, 'Pandu Setiawan', 'ndusetwn@gmail.com', 'default.jpg', '$2y$10$NoKgP0lYWVtt041s1gRKve3GPU8Uz5DlBsbVpUqHl2Nl.egvHxOxW', 2, 1, 1646882437, '15', 1, '2022-03-11 14:08:10', 0, '2022-03-14 14:16:17', 0),
(14, 'Dony Wirawan', 'Dondon@gmail.com', 'default.jpg', '$2y$10$OOhoVNNLi.jbqLoWdKDZAeY0QBhDmRgDXF2Gm/yE38u4Scq8Y1Im2', 2, 0, 1646896761, '16', 14, '2022-03-11 14:10:42', 0, '2022-03-14 14:16:21', 5),
(15, 'Wkwk Testing', 'wkwkwk@gmail.com', 'default.jpg', '$2y$10$jMLmwyeW2sIlqrnjfhKQ3uTQZufcs65Yj.rNHvzj9NPcFtLhwp6S2', 2, 1, 1646982832, '17', 1, '2022-03-11 14:13:52', 0, '2022-03-14 14:16:25', 5),
(16, 'Doddy Ferdiansyah', 'doddy@gmail.com', 'avatar-s-1.jpg', '$2y$10$9jH0sVB5zx9ebN.YYLKDZu9/rIf9JPVQCBDM/cLCLOGyVHLMYSHI2', 2, 1, 1552285263, '18', 2, '2022-03-11 14:08:10', 0, '2022-03-14 14:16:28', 5),
(17, 'Sandhika Galih', 'sandhikagalih@gmail.com', 'default.jpg', '$2y$10$9jH0sVB5zx9ebN.YYLKDZu9/rIf9JPVQCBDM/cLCLOGyVHLMYSHI2', 2, 1, 1553151354, '19', 1, '2022-03-11 14:08:10', 0, '2022-03-14 14:16:31', 0),
(18, 'Pandu Setiawan', 'pandoe@gmail.com', 'user-11.jpg', '$2y$10$ResDu79ZW0fQqhiVtCD1Duijah8Tqfh5uCueZstroGpBzx5TXR642', 2, 1, 1646876469, '20', 1, '2022-03-11 14:08:10', 0, '2022-03-14 14:16:35', 0),
(19, 'Pandu Setiawan', 'ndusetwn@gmail.com', 'default.jpg', '$2y$10$NoKgP0lYWVtt041s1gRKve3GPU8Uz5DlBsbVpUqHl2Nl.egvHxOxW', 2, 1, 1646882437, '21', 1, '2022-03-11 14:08:10', 0, '2022-03-14 14:16:39', 0),
(20, 'Dony Wirawan', 'Dondon@gmail.com', 'default.jpg', '$2y$10$OOhoVNNLi.jbqLoWdKDZAeY0QBhDmRgDXF2Gm/yE38u4Scq8Y1Im2', 2, 0, 1646896761, '22', 14, '2022-03-11 14:10:42', 0, '2022-03-14 14:16:44', 5),
(21, 'Wkwk Testing', 'wkwkwk@gmail.com', 'default.jpg', '$2y$10$jMLmwyeW2sIlqrnjfhKQ3uTQZufcs65Yj.rNHvzj9NPcFtLhwp6S2', 2, 1, 1646982832, '23', 1, '2022-03-11 14:13:52', 0, '2022-03-14 14:16:47', 5),
(22, 'Testeng', 'asdas@asd.asd', 'default.jpg', '$2y$10$DYWm15JOxlUXsn3hiiE3Mey.arQ4jsGs1L2WUQillY1QAG/8qQ.9y', 2, 1, 1647315767, '00', 13, '2022-03-15 10:42:47', 0, '2022-03-15 10:42:47', 0),
(23, 'asd', 'asd@asdasd.asd', 'default.jpg', '$2y$10$vsvp0vUXWQBQSy/h9FeMwujz1pTvnDg0MQdXLg8O7Hpbmjbhl/kAC', 2, 1, 1647315892, '44221', 13, '2022-03-15 10:44:52', 0, '2022-03-15 10:44:52', 0),
(24, 'asd', 'asd@asdasd.asd', 'default.jpg', '$2y$10$pSsmoXYcgQW0EGU/GRxh...8puaLxU211UkuVKfURZCf.Y8QhjuPq', 2, 1, 1647315943, '44222', 1, '2022-03-15 10:45:43', 0, '2022-03-15 10:45:43', 0),
(25, 'dsa', 'dsa@dsa.dsa', 'default.jpg', '$2y$10$fsI62bhVAAiB2gSYoegCC.TGSSo//BUUIwgcpcrIe8JgoBe7qx2k.', 2, 1, 1647315997, '6677', 13, '2022-03-15 10:46:37', 0, '2022-03-15 10:46:48', 5);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_access_menu`
--

CREATE TABLE `user_access_menu` (
  `id` int(11) NOT NULL,
  `role_id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_access_menu`
--

INSERT INTO `user_access_menu` (`id`, `role_id`, `menu_id`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 1, 1, '2022-03-11 15:12:33', 0, '2022-03-11 15:12:33', 0),
(7, 1, 3, '2022-03-11 15:12:33', 0, '2022-03-11 15:12:33', 0),
(8, 1, 2, '2022-03-11 15:12:33', 0, '2022-03-11 15:12:33', 0),
(14, 2, 2, '2022-03-11 15:12:33', 0, '2022-03-11 15:12:33', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_menu`
--

CREATE TABLE `user_menu` (
  `id` int(11) NOT NULL,
  `menu` varchar(128) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_menu`
--

INSERT INTO `user_menu` (`id`, `menu`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Admin', '2022-03-02 13:35:52', 0, '2022-03-15 09:38:09', 0),
(2, 'User', '2022-03-05 13:35:52', 0, '2022-03-11 15:07:28', 0),
(3, 'Menu', '2022-03-03 13:35:52', 0, '2022-03-14 14:12:12', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_role`
--

CREATE TABLE `user_role` (
  `id` int(11) NOT NULL,
  `role` varchar(128) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_role`
--

INSERT INTO `user_role` (`id`, `role`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 'Administrator', '2022-03-11 15:17:52', 0, '2022-03-14 14:11:17', 0),
(2, 'Member', '2022-03-11 15:17:52', 0, '2022-03-11 15:17:52', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_sub_menu`
--

CREATE TABLE `user_sub_menu` (
  `id` int(11) NOT NULL,
  `menu_id` int(11) NOT NULL,
  `title` varchar(128) NOT NULL,
  `url` varchar(128) NOT NULL,
  `icon` varchar(128) NOT NULL,
  `is_active` int(1) NOT NULL,
  `create_date` datetime NOT NULL DEFAULT current_timestamp(),
  `create_by` int(11) NOT NULL DEFAULT 0,
  `update_date` datetime NOT NULL DEFAULT current_timestamp() ON UPDATE current_timestamp(),
  `update_by` int(11) NOT NULL DEFAULT 0
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Dumping data untuk tabel `user_sub_menu`
--

INSERT INTO `user_sub_menu` (`id`, `menu_id`, `title`, `url`, `icon`, `is_active`, `create_date`, `create_by`, `update_date`, `update_by`) VALUES
(1, 1, 'Dashboard', 'admin', 'bx bxs-dashboard', 1, '2022-03-11 15:08:45', 0, '2022-03-14 11:33:25', 0),
(2, 2, 'My Profile', 'user', 'bx bx-user', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(3, 2, 'Edit Profile', 'user/edit', 'bx bx-group', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(4, 3, 'Menu Management', 'menu', 'bx bxs-collection', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(5, 3, 'Submenu Management', 'menu/submenu', 'bx bxs-copy', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(7, 1, 'Role', 'admin/role', 'bx bxs-user-detail', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(8, 2, 'Change Password', 'user/changepassword', 'bx bx-key', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0),
(9, 1, 'Worker', 'admin/worker', 'bx bxs-user-check', 1, '2022-03-11 15:08:45', 0, '2022-03-11 15:08:45', 0);

-- --------------------------------------------------------

--
-- Struktur dari tabel `user_token`
--

CREATE TABLE `user_token` (
  `id` int(11) NOT NULL,
  `email` varchar(128) NOT NULL,
  `token` varchar(128) NOT NULL,
  `date_created` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Indexes for dumped tables
--

--
-- Indeks untuk tabel `departement`
--
ALTER TABLE `departement`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user`
--
ALTER TABLE `user`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_role`
--
ALTER TABLE `user_role`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  ADD PRIMARY KEY (`id`);

--
-- Indeks untuk tabel `user_token`
--
ALTER TABLE `user_token`
  ADD PRIMARY KEY (`id`);

--
-- AUTO_INCREMENT untuk tabel yang dibuang
--

--
-- AUTO_INCREMENT untuk tabel `departement`
--
ALTER TABLE `departement`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=20;

--
-- AUTO_INCREMENT untuk tabel `user`
--
ALTER TABLE `user`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=26;

--
-- AUTO_INCREMENT untuk tabel `user_access_menu`
--
ALTER TABLE `user_access_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=15;

--
-- AUTO_INCREMENT untuk tabel `user_menu`
--
ALTER TABLE `user_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT untuk tabel `user_role`
--
ALTER TABLE `user_role`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT untuk tabel `user_sub_menu`
--
ALTER TABLE `user_sub_menu`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=10;

--
-- AUTO_INCREMENT untuk tabel `user_token`
--
ALTER TABLE `user_token`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
